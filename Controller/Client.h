//
// Created by pedram on 7/7/17.
//

#ifndef AGARIOCLIENT_CLIENT_H
#define AGARIOCLIENT_CLIENT_H


#include <QtCore/QByteArray>
#include <QtCore/QArgument>
#include <QtNetwork/QHostAddress>
#include <QObject>

#include "ConnectionHandler.h"
#include "../Constants.h"

class Client : public QObject
{
Q_OBJECT

public:
    static void onDisconnect();
    static void onConnect();

    static Client *getInstance();
    static void run();

    static bool sendMessage(const std::string &message);
    static void onNewMessage(QByteArray message);

signals:
    void newMessageArrive(QByteArray &message);

private:
    Client();
    ~Client();

};


#endif //AGARIOCLIENT_CLIENT_H

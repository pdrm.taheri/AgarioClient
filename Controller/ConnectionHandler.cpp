//
// Created by pedram on 3/15/17.
//

#include "ConnectionHandler.h"

// Definition of static variables
static ConnectionHandler *instance;

ConnectionHandler *ConnectionHandler::getInstance()
{
    if (instance == nullptr)
    {
        instance = new ConnectionHandler();
    }

    return instance;
}

ConnectionHandler::ConnectionHandler()
{
    connection = new QTcpSocket();
    connect(connection, &QTcpSocket::disconnected, this, &ConnectionHandler::onSocketClosed);
    connect(connection, &QTcpSocket::readyRead, this, &ConnectionHandler::onNewWriteOnSocket);
    connect(connection, &QTcpSocket::connected, this, &ConnectionHandler::onSocketOpened);
}

ConnectionHandler::~ConnectionHandler()
{
    delete instance;
    delete connection;
}

void ConnectionHandler::onNewWriteOnSocket()
{
    onNewMessageHandler(connection->readLine());
    connection->readAll();
}

void ConnectionHandler::setOnNewMessageHandler(void (*handler)(QByteArray))
{
    qDebug() << "New message handler set";
    onNewMessageHandler = handler;
}

void ConnectionHandler::connectToHost(QHostAddress &hostAddress, unsigned short port)
{
    connection->connectToHost(hostAddress, port);
}

void ConnectionHandler::onSocketClosed()
{
    onConnectionClosedHandler();
    qDebug() << "Connection about to close: ";
}

void ConnectionHandler::setOnConnectionClosedHandler(void (*handler)())
{
    qDebug() << "Connection about to close handler set";
    onConnectionClosedHandler = handler;
}

void ConnectionHandler::setOnConnectionEstablishedHandler(void (*handler)())
{
    qDebug() << "Connection about to close handler set";
    onConnectionEstablishedHandler = handler;
}

void ConnectionHandler::onSocketOpened()
{
    onConnectionEstablishedHandler();
}

void ConnectionHandler::writeOnSocket(QByteArray message)
{
    connection->write(message);
}


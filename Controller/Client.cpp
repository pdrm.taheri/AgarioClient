//
// Created by pedram on 7/7/17.
//

#include "Client.h"


// Definition of static variables
static Client *instance;

Client::Client()
{
    ConnectionHandler::getInstance()->setOnConnectionClosedHandler(Client::onDisconnect);
    ConnectionHandler::getInstance()->setOnConnectionEstablishedHandler(Client::onConnect);
    ConnectionHandler::getInstance()->setOnNewMessageHandler(Client::onNewMessage);
}

Client::~Client()
{
    delete instance;
}

void Client::onNewMessage(QByteArray message)
{
    qDebug() << "New message received " << message;

    emit getInstance()->newMessageArrive(message);
}

void Client::onDisconnect()
{

}

void Client::onConnect()
{

}

Client *Client::getInstance()
{
    if (instance == nullptr)
    {
        instance = new Client();
    }

    return instance;
}

void Client::run()
{
    getInstance(); // To initialize handlers

    QHostAddress address(HOST_ADDRESS);
    ConnectionHandler::getInstance()->connectToHost(address, HOST_PORT);
}

bool Client::sendMessage(const std::string &text)
{
    qDebug() << "Sending message";

    ConnectionHandler::getInstance()->writeOnSocket(text.c_str());

    return true;
}


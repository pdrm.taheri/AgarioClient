//
// Created by Pedram Taheri on 7/27/2017 AD.
//

#include <sstream>
#include "Parser.h"

std::vector<std::string> split(const std::string &s, char delim)
{
    std::vector<std::string> elements;
    std::stringstream ss;
    ss.str(s);
    std::string item;

    while (std::getline(ss, item, delim))
    {
        *(std::back_inserter(elements)++) = item;
    }

    return elements;
}

GameBoard *Parser::parseFromBytes(QByteArray &bytes)
{
    GameBoard *parsed = new GameBoard();

    bytes.remove(0, 1);

    std::vector<std::string> parsedObjects = split(bytes.toStdString(), DELIMITER);
    parsed->myId = std::stoi(parsedObjects[0]);
    for (int i = 1; i < parsedObjects.size() - 1; ++i)
    {
        std::string object = parsedObjects[i];
        std::vector<std::string> parsedObject = split(object, ' ');
        if (parsedObject.size() == 1)
            continue;

        if (parsedObject.size() == 2) // Food
        {
            parsed->addFood(new Food(std::stoi(parsedObject[0]), std::stoi(parsedObject[1])));
        }
        else // Player
        {
            Player *p = new Player(std::stoi(parsedObject[0]), std::stoi(parsedObject[1]), std::stoi(parsedObject[2]), std::stoi(parsedObject[3]));
            parsed->addPlayer(p);
        }
    }

    return parsed;
}

GameBoard Parser::parseFromBytes(GameBoard &board, QByteArray &bytes)
{
    bytes.remove(0, 1);

    std::vector<std::string> parsedObjects = split(bytes.toStdString(), DELIMITER);
    for (int i = 0; i < parsedObjects.size() - 1; ++i)
    {
        std::string object = parsedObjects[i];
        std::vector<std::string> parsedObject = split(object, ' ');
        if (parsedObject.size() == 1)
            continue;

        if (parsedObject.size() == 2) // Food
        {
            board.addFood(new Food(std::stoi(parsedObject[0]), std::stoi(parsedObject[1])));
        }
        else // Player
        {
            Player *p = new Player(std::stoi(parsedObject[0]), std::stoi(parsedObject[1]), std::stoi(parsedObject[2]), std::stoi(parsedObject[3]));
            qDebug() << p->getId();
            board.addPlayer(p);
        }
    }

    return board;
}

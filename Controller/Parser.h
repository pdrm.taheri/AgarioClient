//
// Created by Pedram Taheri on 7/27/2017 AD.
//

#ifndef AGARIOCLIENT_PARSER_H
#define AGARIOCLIENT_PARSER_H


#include "../Model/GameBoard.h"
#include <qdebug.h>

#define DELIMITER '&'

class Parser
{
public:
    static GameBoard *parseFromBytes(QByteArray &bytes);
    static GameBoard parseFromBytes(GameBoard &board, QByteArray &bytes);
};


#endif //AGARIOCLIENT_PARSER_H

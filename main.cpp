//
// Created by pedram on 6/29/17.
//

#include <QtWidgets/QApplication>
#include "View/GameWindow.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    GameWindow window;

    return app.exec();
}

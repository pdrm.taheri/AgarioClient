//
// Created by pedram on 6/29/17.
//

#include "Food.h"

QGraphicsEllipseItem *Food::getShape()
{
    return shape;
}

Food::Food(int x, int y) : GameObject(x, y)
{
    shape = new QGraphicsEllipseItem(x, y, 10, 10);
}

Food::~Food()
{
    delete shape;
}

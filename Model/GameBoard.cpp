//
// Created by pedram on 6/29/17.
//

#include "GameBoard.h"

GameBoard::GameBoard() {  }

GameBoard::~GameBoard()
{
    for (int i = 0; i < objects.size(); ++i)
    {
        delete objects[i];
        objects[i] = nullptr;
    }
}

void GameBoard::addPlayer(Player *p)
{
    objects.push_back(p);
    players.push_back(p);
}

void GameBoard::addFood(Food *f)
{
//    objects.push_back(f);
    foods.push_back(f);
}

void GameBoard::addHideout(Hideout *h)
{
    objects.push_back(h);
    hideouts.push_back(h);
}

std::vector<GameObject *> GameBoard::getObjects()
{
    return objects;
}

std::vector<Hideout *> GameBoard::getHideouts()
{
    return hideouts;
}

std::vector<Player *> GameBoard::getPlayers()
{
    return players;
}

std::vector<Food *> GameBoard::getFoods()
{
    return foods;
}

//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_FOOD_H
#define AGARIOCLIENT_FOOD_H


#include "GameObject.h"

class Food : public GameObject
{
public:
    Food(int x, int y);
    ~Food();

    QGraphicsEllipseItem *getShape();

private:
    QGraphicsEllipseItem *shape;
};


#endif //AGARIOCLIENT_FOOD_H

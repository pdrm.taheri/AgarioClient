//
// Created by pedram on 6/29/17.
//

#include "GameObject.h"

QPoint *GameObject::getLocation()
{
    return location;
}

GameObject::GameObject(int x, int y)
{
    location = new QPoint(x, y);
}

GameObject::~GameObject()
{
    delete location;
}

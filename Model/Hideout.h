//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_HIDEOUT_H
#define AGARIOCLIENT_HIDEOUT_H


#include "GameObject.h"

class Hideout : public GameObject
{
public:
    Hideout(int x, int y);

};


#endif //AGARIOCLIENT_HIDEOUT_H

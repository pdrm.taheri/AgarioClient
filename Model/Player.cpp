//
// Created by pedram on 6/29/17.
//

#include "Player.h"

QGraphicsEllipseItem *Player::getShape()
{
    return shape;
}

Player::~Player()
{
    delete shape;
    shape = nullptr;
}

Player::Player(int x, int y, int radius, int id) : GameObject(x, y), id(id)
{
    shape = new QGraphicsEllipseItem(x, y, radius, radius);
}

int Player::getId()
{
    return id;
}

//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_PLAYER_H
#define AGARIOCLIENT_PLAYER_H


#include "GameObject.h"
#include <qdebug.h>

class Player : public GameObject
{

public:
    Player(int x, int y, int radius, int id);
    ~Player();

    QGraphicsEllipseItem *getShape();
    int getId();

private:
    int id;
    QGraphicsEllipseItem *shape;
};


#endif //AGARIOCLIENT_PLAYER_H

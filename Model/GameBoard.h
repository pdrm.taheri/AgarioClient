//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_GAMEBOARD_H
#define AGARIOCLIENT_GAMEBOARD_H


#include <vector>
#include "Player.h"
#include "Hideout.h"
#include "Food.h"

class GameBoard
{
public:
    GameBoard();
    ~GameBoard();

    void addPlayer(Player *p);
    void addFood(Food *f);
    void addHideout(Hideout *h);

    std::vector<GameObject *> getObjects();
    std::vector<Hideout *> getHideouts();
    std::vector<Player *> getPlayers();
    std::vector<Food *> getFoods();


//private:
    std::vector<GameObject *> objects;
    std::vector<Hideout *> hideouts;
    std::vector<Player *> players;
    std::vector<Food *> foods;

    int myId;
};


#endif //AGARIOCLIENT_GAMEBOARD_H

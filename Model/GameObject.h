//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_GAMEOBJECT_H
#define AGARIOCLIENT_GAMEOBJECT_H


#include <QtWidgets/QGraphicsEllipseItem>

class GameObject
{
public:
    GameObject(int x, int y);
    virtual ~GameObject();

    QPoint *getLocation();

private:
    QPoint *location;
};


#endif //AGARIOCLIENT_GAMEOBJECT_H

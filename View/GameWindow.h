//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_GAMEWINDOW_H
#define AGARIOCLIENT_GAMEWINDOW_H


#include <QtWidgets/QGraphicsView>
#include <QMouseEvent>
#include <QKeyEvent>
#include "../Model/GameBoard.h"
#include "../Controller/Client.h"

class GameWindow : public QGraphicsView
{
Q_OBJECT

public:
    GameWindow();
    ~GameWindow();

    void mouseMoveEvent (QMouseEvent * event);
    void keyPressEvent (QKeyEvent *event);

private:
    void onDataReceived(QByteArray &data);
    void updateSceneFromBoard(GameBoard board);

    QGraphicsScene *scene;
    GameBoard *board;

    void setupScene();
};


#endif //AGARIOCLIENT_GAMEWINDOW_H

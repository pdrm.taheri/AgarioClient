//
// Created by pedram on 6/29/17.
//

#include "GameWindow.h"
#include "../Controller/Parser.h"
#include <QScrollBar>

GameWindow::GameWindow()
{
    scene = new QGraphicsScene(this);
    setScene(scene);

    scene->addItem(new QGraphicsLineItem(0, 0, 2000, 0));
    scene->addItem(new QGraphicsLineItem(0, 2000, 2000, 2000));
    scene->addItem(new QGraphicsLineItem(0, 0, 0, 2000));
    scene->addItem(new QGraphicsLineItem(2000, 0, 2000, 2000));

    setMouseTracking(true);

    Client::getInstance()->run();
    connect(Client::getInstance(), &Client::newMessageArrive, this, &GameWindow::onDataReceived);

    verticalScrollBar()->setEnabled(false);
    horizontalScrollBar()->setEnabled(false);

    setFixedSize(500, 500);

    show();
}

void GameWindow::mouseMoveEvent (QMouseEvent * event)
{
    //Show x and y coordinate values of mouse cursor here
    if (rand() % 5 == 1)
    {
        QPoint origin = mapFromGlobal(QCursor::pos());
        QPointF pos = mapToScene(origin);
        Client::getInstance()->sendMessage(std::to_string(pos.x()) + " " + std::to_string(pos.y()) + "\n");
    }

//    if(event->buttons() == Qt::LeftButton)

}

GameWindow::~GameWindow()
{
    scene->clear();

    delete board;
    delete scene;
}

void GameWindow::onDataReceived(QByteArray &data)
{
    if (data.toStdString()[0] == 'B') // GameBoard
    {
        delete board;
        scene->clear();
        board = Parser::parseFromBytes(data);
        setupScene();
//        updateSceneFromBoard(*board);
//        delete board;
    }
    else if  (data.toStdString()[0] == 'S') // Initial Board
    {
        board = Parser::parseFromBytes(data);
//        this->board.objects = board.objects;
//        this->board.foods = board.foods;
//        this->board.players = board.players;
//        this->board.hideouts = board.hideouts;
        setupScene();
    }
    else if (data.toStdString()[0] == 'D') // Player death
    {

    }
    else // New Player
    {

    }
//
//
//    qDebug() << "Data Recieved!";
}

void GameWindow::updateSceneFromBoard(GameBoard board)
{
    for (Player *p1 : board.getPlayers())
    {
//        qDebug() << p1->getLocation()->x() << p1->getLocation()->y();
//        qDebug() << this->board->getPlayers()[p1->getId()]->getShape();// << this->board.getPlayers()[p1->getId()];
        int dx = p1->getLocation()->x() - this->board->getPlayers()[p1->getId()]->getShape()->x();
        int dy = p1->getLocation()->y() - this->board->getPlayers()[p1->getId()]->getShape()->y();
        this->board->getPlayers()[p1->getId()]->getShape()->moveBy(dx, dy);
    }
}

void GameWindow::setupScene()
{
    for (Player *obj : board->getPlayers())
        scene->addItem(obj->getShape());

    for (Food *obj : board->getFoods())
        scene->addItem(obj->getShape());

    scene->addItem(new QGraphicsLineItem(0, 0, 2000, 0));
    scene->addItem(new QGraphicsLineItem(0, 2000, 2000, 2000));
    scene->addItem(new QGraphicsLineItem(0, 0, 0, 2000));
    scene->addItem(new QGraphicsLineItem(2000, 0, 2000, 2000));

    scene->setSceneRect(this->board->getPlayers()[this->board->myId]->getShape()->rect());
}

void GameWindow::keyPressEvent(QKeyEvent *event)
{
    switch ( tolower(event->key()) ) {
        case 's':
            Client::getInstance()->sendMessage("Shoot\n");
            break;
        case 'q':
            break;
    }
}


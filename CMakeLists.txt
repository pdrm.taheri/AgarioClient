cmake_minimum_required(VERSION 3.7)
project(AgarioClient)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_AUTOMOC ON)

set(CMAKE_PREFIX_PATH /opt/Qt5.8.0/5.8/gcc_64/)

# Find Qt libraries needed
find_package(Qt5Network)
find_package(Qt5Widgets)

set(SOURCE_FILES main.cpp Model/GameBoard.cpp Model/GameBoard.h Model/GameObject.cpp Model/GameObject.h Model/Food.cpp Model/Food.h Model/Hideout.cpp Model/Hideout.h Model/Player.cpp Model/Player.h View/GameWindow.cpp View/GameWindow.h Controller/Client.cpp Controller/Client.h Controller/ConnectionHandler.h Controller/ConnectionHandler.cpp Constants.h Controller/Parser.cpp Controller/Parser.h)

add_executable(AgarioClient ${SOURCE_FILES})

target_link_libraries(AgarioClient Qt5::Network)
target_link_libraries(AgarioClient Qt5::Widgets)